import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/login.vue'
import Home from './views/Home.vue'
import Membros from './components/membros/membros.vue'
import Perfil from './components/perfil/perfil.vue'
import Dashboard from './components/Dashboard'
import Newslater from './components/newslater/newslater.vue'
import Funcionario from './components/funcionarios/funcionario.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/',
      name: 'home',
      component: Home,
      children: [
        {
          path: '/dashboard',
          name: '/dashboard',
          component: Dashboard
        },
        {
          path: '/membros',
          name: 'membros',
          component: Membros
        },
        {
          path: '/newslater',
          name: 'newslater',
          component: Newslater
        },
        {
          path: '/perfil',
          name: 'perfil',
          component: Perfil
        },
        {
          path: '/funcionarios',
          name: 'funcionarios',
          component: Funcionario
        }

      ]
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
