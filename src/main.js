import Vue from 'vue'
import App from './App.vue'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'



import VEditor from 'yimo-vue-editor'

Vue.use(BootstrapVue)
Vue.use (VEditor, {
  name: 'v-editor-app',
  config: {},
  uploaHandler: (type, resTxt) => {
    if(type === 'success') {
      var res = JSON.parse(resTxt)
      if (res.status !==1 ) {
        return null
      }
      return res.data
    } else if (type === 'error'){

    } else if (type === 'timeout'){

    }
    return 'upload failed__'
  }
})
Vue.config.productionTip = false


new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
